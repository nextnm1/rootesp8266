//************************************************************
// this is a simple example that uses the painlessMesh library
//
// 1. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 2. prints anything it receives to Serial.print
//
//
//************************************************************
#include "painlessMesh.h"
#include <ArduinoJson.h>
#include <string>
#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

Scheduler userScheduler; // to control your personal task
painlessMesh  mesh;
String msg="";
// User stub




// void sendMessage() ; // Prototype so PlatformIO doesn't complain
//
// Task taskSendMessage( TASK_SECOND * 1 , TASK_FOREVER, &sendMessage );
//
// void sendMessage() {
//
//   // msg += mesh.getNodeId();
//   // mesh.sendBroadcast( msg );
//   // taskSendMessage.setInterval( random( TASK_SECOND * 1, TASK_SECOND * 5 ));
// 	// Serial.println(msg);
// }



// Needed for painless library
void receivedCallback( uint32_t from, String &msg ) {
  //Serial.printf("Received from %u msg=%s\n", from, msg.c_str());
  int i=0;
  char buffer [20];
  char buffer2 [msg.length()+1];
  itoa (from,buffer,10);
  std::string dadosStr="";
  dadosStr.append(buffer).c_str();
  dadosStr.append(";");
  msg.toCharArray(buffer2, msg.length()+1);
  dadosStr.append(buffer2);
  Serial.print(dadosStr.c_str());

  // std::string dadosStr=msg;
  //dadosStr.append(msg).c_str();

  // StaticJsonBuffer<1024> jsonBuffer;
  // JsonObject& bridge= jsonBuffer.createObject();
  // bridge["node"]=from;
  // JsonArray& sensors=  bridge.createNestedArray("sensors");
  // JsonArray& actions=  bridge.createNestedArray("actions");
  // JsonArray& configs=  bridge.createNestedArray("configs");
  // // sensor["name"]="luz";
  // // sensor["value"]=12;
  // // String aux;
  // JsonObject& node = jsonBuffer.parseObject(msg);
  // node["node"]=from;

  //root.printTo(Serial);

//    for (i=0;i<node["sensors"].size();i++){
//      JsonObject& sensor=  sensors.createNestedObject();
//      sensor= node["sensors"][i].;
//      // sensor["name"]=node["sensors"][i]["name"];
//      // sensor["value"]=node["sensors"][i]["value"];
//   }
//   for (i=0;i<node["actions"].size();i++){
//     JsonObject& action=  actions.createNestedObject();
//     action=node["actions"].asObject();
//     // action["name"]=node["actions"][i]["name"];
//     // action["value"]=node["actions"][i]["value"];
//  }
//
//  for (i=0;i<node["configs"].size();i++){
//    JsonObject& config=  configs.createNestedObject();
//    config=node["configs"][i].asObject();
//    // config["name"]=node["configs"][i]["name"];
//    // config["value"]=node["configs"][i]["value"];
// }

//Serial.print(dadosStr.c_str());
  //root.printTo(Serial);
  //Serial.println(root.size());
  // jsonBuffer.clear();


}


void newConnectionCallback(uint32_t nodeId) {
    //Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
    //Serial.printf("Changed connections %s\n",mesh.subConnectionJson().c_str());
}

void nodeTimeAdjustedCallback(int32_t offset) {
    //Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(),offset);
}

void setup() {
  Serial.begin(115200);

//mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  // userScheduler.addTask( taskSendMessage );
  // taskSendMessage.enable();
  //taskSendMessage.disable();

}
void receiveOrder(){ // verifica se vem algo do servidor por Serial e repassa para o nó destino

  if(Serial.available()>0){
    StaticJsonBuffer<512> jsonBuffer;
    String msg_;
    msg="hello from  ";
    delay(100);
    msg = "";
	  msg_ = Serial.readStringUntil('\n');
    JsonObject& root = jsonBuffer.parseObject(msg_);
	  //Serial.print("CEnas ");
    uint32_t dest = root["node"];
    String payload ="";
    root["msg"].printTo(payload);


    //Serial.println(payload);
    if(dest!=0){
      Serial.print(mesh.sendSingle(dest,  payload)); // Ver documentação dos returns. false -se falha. true- se corre bem
		  // msg=String(msg_);

    }
  }

}

void loop() {
//  Serial.println(mesh.getNodeId());
  userScheduler.execute(); // it will run mesh scheduler as well
  mesh.update();
  receiveOrder();

}
